/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef ANTENNA_H_
#define ANTENNA_H_

#include "main.h"
#include "watchdog.h"


/** Time to test antenna deploy mechanism in ms */
#define ANT_DEPLOY_TEST_TIME		200
/** Lower limit of antenna deploy current in mA */
#define ANT_DEPLOY_TEST_CURRENT		500
/** Max time in seconds to keep antenna deploy powered*/
#define ANT_DEPLOY_TIME				120
/** Time in ms to keep deployment active after deployment detection */
#define ANTENNA_POST_DEPLOY_DELAY 	1000
/**
 * Number of consecutive reads of the antenna status register for debouncing
 */
#define ANT_STATUS_DEBOUNCE_CNT     20


/**
 * @brief Antenna deploy commands
 */
typedef enum {
	ANT_DEPLOY_ON = SET,	//!< ANT_DEPLOY_ON
	ANT_DEPLOY_OFF = RESET	//!< ANT_DEPLOY_OFF
} ant_deploy_power_t;

/**
 * @brief Antenna deploy states
 */
typedef enum {
	ANT_STOWED,				//!< Antenna is stowed
	ANT_DEPLOYED,			//!< Antenna is deployed
	ANT_ASSUMED_DEPLOYED	//!< Antenna is deployed
} ant_deploy_status_t;

/**
 * @brief Antenna deploy test states
 */
typedef enum {
	ANT_DEPLOY_TEST_OK,      	//!< Deploy test was successful
	ANT_DEPLOY_TEST_DEGRADED,	//!< Deploy system is degraded.
	ANT_DEPLOY_TEST_FAIL,     	//!< Deploy test has failed
	ANT_DEPLOY_TEST_PENDING		//!< Deploy test pending
} ant_deploy_test_status_t;

/**
 * @brief Antenna deploy status
 */
struct ant_status {
	ant_deploy_test_status_t ant_deploy_test : 8;	//!< Antenna deploy test result
	ant_deploy_status_t ant_deploy_status : 8;	//!< Antenna deploy status
	uint8_t antenna_deploy_time;			//!< Antenna deploy elapsed time. Time in seconds it took for the antenna to deploy
	uint8_t antenna_retry_count;			//!< Antenna deploy retry counter
} __attribute__((packed));

struct ant_power_stats {
	int16_t ant_deploy_test_current; 	/**< Relative current draw during antenna deployment test */
	int16_t ant_deploy_test_voltage; 	/**< Voltage during antenna deployment test */
	int16_t ant_deploy_avg_current; 	/**< Average current draw during antenna deployment */
	int16_t ant_deploy_avg_voltage; 	/**< Average Voltage during antenna deployment */
} __attribute__((packed));

ant_deploy_test_status_t
antenna_deploy_test();
ant_deploy_status_t
antenna_deploy_status();
uint8_t
antenna_deploy(struct watchdog *hwdg, uint8_t wdgid);

#endif /* ANTENNA_H_ */
