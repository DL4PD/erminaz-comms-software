/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bsp_pq9ish_comms.h"


/**
 * Controls power amplifier state (on/off)
 * @param hax AX5043 handler
 * @param state power state
 */
void
enable_pa(struct ax5043_conf *hax)
{
	if (!hax) {
		return;
	}
	ax5043_set_pwramp(hax, PA_ENABLE);
}

void
disable_pa(struct ax5043_conf *hax)
{
	if (!hax) {
		return;
	}
	ax5043_set_pwramp(hax, PA_DISABLE);
}

void
pq9ish_can_power(pq9ish_can_power_t state)
{
	switch (state) {
		case POWER_OFF:
//      HAL_GPIO_WritePin(CAN_EN_GPIO_Port, CAN_EN_Pin, SET);
			break;
		case POWER_ON:
//      HAL_GPIO_WritePin(CAN_EN_GPIO_Port, CAN_EN_Pin, RESET);
			break;
		case POWER_STANBY:
//      HAL_GPIO_WritePin(CAN_STB_GPIO_Port, CAN_STB_Pin, SET);
			break;
	}
}
