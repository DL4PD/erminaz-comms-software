options:
  parameters:
    author: Manolis Surligas (surligas@gmail.com)
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: Qubik Transceiver
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: no_gui
    hier_block_src_path: '.:'
    id: qubik_receiver
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: qubik_receiver
    window_size: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 4.0]
    rotation: 0
    state: enabled

blocks:
- name: audio_samp_rate
  id: variable
  parameters:
    comment: ''
    value: '48000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1440, 12.0]
    rotation: 0
    state: enabled
- name: decimation
  id: variable
  parameters:
    comment: ''
    value: max(4,satnogs.find_decimation(baudrate, 2, audio_samp_rate))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1248, 12.0]
    rotation: 0
    state: true
- name: rx_freq
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: RX Frequency
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: rx_freq-10e3
    step: 1e3
    stop: rx_freq+10e3
    value: 435.240e6
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1568, 12.0]
    rotation: 0
    state: disabled
- name: variable_cc_decoder_def_0
  id: variable_cc_decoder_def
  parameters:
    comment: ''
    dim1: '1'
    dim2: '1'
    framebits: (128+32+4)*8
    k: '7'
    mode: fec.CC_STREAMING
    ndim: '0'
    padding: 'False'
    polys: '[79, -109]'
    rate: '2'
    state_end: '-1'
    state_start: '0'
    value: '"ok"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1928, 284.0]
    rotation: 0
    state: enabled
- name: variable_ieee802_15_4_variant_decoder_0
  id: variable_ieee802_15_4_variant_decoder
  parameters:
    comment: ''
    crc: satnogs.crc.CRC32_C
    drop_invalid: 'False'
    frame_len: 128+32
    preamble: '[0x33]*4'
    preamble_thrsh: '12'
    rs: 'True'
    sync_thrsh: '3'
    sync_word: '[0x3C, 0x67, 0x49, 0x52]'
    var_len: 'False'
    whitening: satnogs.whitening.make_ccsds(True)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1713, 16]
    rotation: 0
    state: true
- name: variable_ieee802_15_4_variant_decoder_cc
  id: variable_ieee802_15_4_variant_decoder
  parameters:
    comment: "Allow more wrong bits on the sync word,\n for the Viterbi paths to settle"
    crc: satnogs.crc.CRC32_C
    drop_invalid: 'True'
    frame_len: 128+32
    preamble: '[0x33]*4'
    preamble_thrsh: '12'
    rs: 'True'
    sync_thrsh: '7'
    sync_word: '[0x3C, 0x67, 0x49, 0x52]'
    var_len: 'False'
    whitening: satnogs.whitening.make_ccsds(True)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1896, 16.0]
    rotation: 0
    state: true
- name: analog_quadrature_demod_cf_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.0'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1264, 524.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0_0_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.0'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [496, 524.0]
    rotation: 0
    state: enabled
- name: antenna_rx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [906, 13]
    rotation: 0
    state: enabled
- name: baudrate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '9600.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1352, 12.0]
    rotation: 0
    state: enabled
- name: bb_freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Baseband CORDIC frequency (if the device supports it)
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1760, 268.0]
    rotation: 0
    state: enabled
- name: blocks_delay_0
  id: blocks_delay
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    delay: 1024//2
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [536, 452.0]
    rotation: 0
    state: enabled
- name: blocks_message_debug_0
  id: blocks_message_debug
  parameters:
    affinity: ''
    alias: ''
    comment: If in doubt, print it out!
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [832, 312.0]
    rotation: 0
    state: disabled
- name: blocks_moving_average_xx_0
  id: blocks_moving_average_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    max_iter: '4096'
    maxoutbuf: '0'
    minoutbuf: '0'
    scale: 1.0/1024.0
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [648, 500.0]
    rotation: 0
    state: enabled
- name: blocks_multiply_const_vxx_0
  id: blocks_multiply_const_vxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    const: 1/(math.pi*1.1)
    maxoutbuf: '0'
    minoutbuf: '0'
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1880, 788.0]
    rotation: 180
    state: true
- name: blocks_multiply_xx_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [968, 456.0]
    rotation: 0
    state: enabled
- name: blocks_vco_c_0
  id: blocks_vco_c
  parameters:
    affinity: ''
    alias: ''
    amplitude: '1.0'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    sensitivity: -baudrate*decimation
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [800, 508.0]
    rotation: 0
    state: enabled
- name: bw
  id: parameter
  parameters:
    alias: ''
    comment: 'The bandwidth should configure RF filters on some devices.

      Set to 0.0 for automatic calculation.'
    hide: none
    label: Bandwidth
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1120, 12.0]
    rotation: 0
    state: enabled
- name: dc_blocker_xx_0
  id: dc_blocker_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    long_form: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: ff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1624, 516.0]
    rotation: 0
    state: enabled
- name: dc_removal
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Remove automatically the DC offset (if the device support it)
    short_id: ''
    type: str
    value: '"False"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1616, 268.0]
    rotation: 0
    state: enabled
- name: decoded_data_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: /tmp/.satnogs/data/data
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 860.0]
    rotation: 0
    state: enabled
- name: dev_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device arguments
    short_id: ''
    type: str
    value: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [520, 12.0]
    rotation: 0
    state: enabled
- name: digital_binary_slicer_fb_0
  id: digital_binary_slicer_fb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1888, 668.0]
    rotation: 180
    state: enabled
- name: digital_clock_recovery_mm_xx_0
  id: digital_clock_recovery_mm_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain_mu: 0.5/8.0
    gain_omega: 2 * math.pi / 100
    maxoutbuf: '0'
    minoutbuf: '0'
    mu: '0.5'
    omega: '2'
    omega_relative_limit: '0.01'
    type: float
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1880, 492.0]
    rotation: 0
    state: enabled
- name: doppler_correction_per_sec
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [176, 860.0]
    rotation: 0
    state: enabled
- name: enable_iq_dump
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [264, 780.0]
    rotation: 0
    state: enabled
- name: fec_extended_decoder_0
  id: fec_extended_decoder
  parameters:
    affinity: ''
    alias: ''
    ann: None
    comment: ''
    decoder_list: variable_cc_decoder_def_0
    maxoutbuf: '0'
    minoutbuf: '0'
    puncpat: '''11'''
    threadtype: capillary
    value: fec_extended_decoder
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1600, 764.0]
    rotation: 180
    state: true
- name: file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: test.wav
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [160, 780.0]
    rotation: 180
    state: enabled
- name: gain_mode
  id: parameter
  parameters:
    alias: ''
    comment: 'Set the gain mode of the Soapy.

      Can be "Overall", "Specific"

      or "Settings Field"'
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"Overall"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1120, 140.0]
    rotation: 0
    state: enabled
- name: gain_rx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1032, 12.0]
    rotation: 0
    state: true
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import math
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 164.0]
    rotation: 0
    state: enabled
- name: iq_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: /tmp/iq.dat
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [408, 780.0]
    rotation: 0
    state: enabled
- name: lo_offset
  id: parameter
  parameters:
    alias: ''
    comment: 'To avoid the SDR carrier at the DC

      we shift the LO a little further'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [808, 13]
    rotation: 0
    state: enabled
- name: low_pass_filter_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: 0.75 * baudrate
    decim: decimation // 2
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    type: fir_filter_ccf
    width: baudrate / 8.0
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1104, 476.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_0_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: "Perform a relaxed filter to increase \nthe performance of the auto frequency\n\
      correction"
    cutoff_freq: baudrate*1.25
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    type: fir_filter_ccf
    width: baudrate / 2.0
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [280, 476.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_1
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: baudrate * 0.60
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: 2 * baudrate
    type: fir_filter_fff
    width: baudrate / 8.0
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1456, 476.0]
    rotation: 0
    state: bypassed
- name: osdlp_ip
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: 127.0.0.1
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [512, 780.0]
    rotation: 0
    state: enabled
- name: other_settings
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Soapy Channel other settings
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1456, 268.0]
    rotation: 0
    state: enabled
- name: ppm
  id: parameter
  parameters:
    alias: ''
    comment: 'The frequency correction in PPM

      to correct for a local oscillator frequency deviation'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1296, 140.0]
    rotation: 0
    state: enabled
- name: qtgui_freq_sink_x_0
  id: qtgui_freq_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    average: '1.0'
    axislabels: 'True'
    bw: samp_rate
    color1: '"blue"'
    color10: '"dark blue"'
    color2: '"red"'
    color3: '"green"'
    color4: '"black"'
    color5: '"cyan"'
    color6: '"magenta"'
    color7: '"yellow"'
    color8: '"dark red"'
    color9: '"dark green"'
    comment: ''
    ctrlpanel: 'False'
    fc: '0'
    fftsize: '1024'
    freqhalf: 'True'
    grid: 'False'
    gui_hint: ''
    label: Relative Gain
    label1: ''
    label10: ''''''
    label2: ''''''
    label3: ''''''
    label4: ''''''
    label5: ''''''
    label6: ''''''
    label7: ''''''
    label8: ''''''
    label9: ''''''
    legend: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '""'
    nconnections: '1'
    showports: 'False'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_tag: '""'
    type: complex
    units: dB
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    wintype: firdes.WIN_BLACKMAN_hARRIS
    ymax: '10'
    ymin: '-140'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [240, 316.0]
    rotation: 180
    state: disabled
- name: qubik_listener_ip
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: 127.0.0.1
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [256, 940.0]
    rotation: 0
    state: enabled
- name: qubik_listener_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '16982'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [376, 940.0]
    rotation: 0
    state: enabled
- name: rigctl_host
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: 127.0.0.1
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 940.0]
    rotation: 0
    state: enabled
- name: rigctl_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '4532'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [120, 940.0]
    rotation: 0
    state: enabled
- name: rx_freq
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 435.240e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1456, 76.0]
    rotation: 0
    state: true
- name: samp_rate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device Sampling rate (common for TX and RX)
    short_id: ''
    type: eng_float
    value: 1e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [656, 12.0]
    rotation: 0
    state: enabled
- name: satnogs_doppler_compensation_0
  id: satnogs_doppler_compensation
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    compensate: '0'
    fine_correction: '0'
    lo_offset: lo_offset
    maxoutbuf: '0'
    minoutbuf: '0'
    out_samp_rate: baudrate*decimation
    samp_rate: samp_rate
    sat_freq: rx_freq
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [704, 148.0]
    rotation: 0
    state: true
- name: satnogs_frame_decoder_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ieee802_15_4_variant_decoder_0
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1152, 672.0]
    rotation: 180
    state: enabled
- name: satnogs_frame_decoder_0_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ieee802_15_4_variant_decoder_cc
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1152, 800.0]
    rotation: 180
    state: true
- name: satnogs_iq_sink_0
  id: satnogs_iq_sink
  parameters:
    activate: enable_iq_dump
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    filename: iq_file_path
    scale: '16768'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 508.0]
    rotation: 180
    state: enabled
- name: satnogs_json_converter_0
  id: satnogs_json_converter
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    extra: '''{"Coding":"RS"}'''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [872, 740.0]
    rotation: 180
    state: true
- name: satnogs_json_converter_0_0
  id: satnogs_json_converter
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    extra: '''{"Coding":"CC-RS"}'''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [872, 804.0]
    rotation: 180
    state: true
- name: satnogs_multi_format_msg_sink_0
  id: satnogs_multi_format_msg_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filepath: ''
    format: '0'
    outstream: 'True'
    timestamp: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [640, 868.0]
    rotation: 180
    state: true
- name: satnogs_tcp_rigctl_msg_source_0
  id: satnogs_tcp_rigctl_msg_source
  parameters:
    addr: rigctl_host
    affinity: ''
    alias: ''
    comment: ''
    interval: '1000'
    maxoutbuf: '0'
    minoutbuf: '0'
    mode: 'False'
    mtu: '1500'
    port: rigctl_port
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [448, 268.0]
    rotation: 0
    state: disabled
- name: satnogs_udp_msg_sink_0
  id: satnogs_udp_msg_sink
  parameters:
    addr: osdlp_ip
    affinity: ''
    alias: ''
    comment: OSDLP
    mtu: '1500'
    port: '16882'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [872, 620.0]
    rotation: 180
    state: enabled
- name: satnogs_udp_msg_sink_0_1
  id: satnogs_udp_msg_sink
  parameters:
    addr: qubik_listener_ip
    affinity: ''
    alias: ''
    comment: SATNOGS
    mtu: '1500'
    port: qubik_listener_port
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [912, 868.0]
    rotation: 180
    state: enabled
- name: soapy_rx_device
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: '"driver=invalid"'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [416, 12.0]
    rotation: 0
    state: enabled
- name: soapy_source_0
  id: soapy_source
  parameters:
    affinity: ''
    agc0: 'False'
    agc1: 'False'
    alias: ''
    amp_gain0: '0'
    ant0: antenna_rx
    ant1: RX2
    args: dev_args
    balance0: '0'
    balance1: '0'
    bw0: bw
    bw1: '0'
    center_freq0: rx_freq - lo_offset
    center_freq1: '0'
    clock_rate: '0'
    clock_source: ''
    comment: ''
    correction0: ppm
    correction1: '0'
    dc_offset0: '0'
    dc_offset1: '0'
    dc_removal0: dc_removal
    dc_removal1: 'True'
    dev: soapy_rx_device
    devname: custom
    gain_mode0: gain_mode
    gain_mode1: Overall
    ifgr_gain: '59'
    lna_gain0: '10'
    lna_gain1: '10'
    maxoutbuf: '0'
    minoutbuf: '0'
    mix_gain0: '10'
    nchan: '1'
    nco_freq0: bb_freq
    nco_freq1: '0'
    overall_gain0: gain_rx
    overall_gain1: '10'
    pga_gain0: '24'
    pga_gain1: '24'
    rfgr_gain: '9'
    rxvga1_gain: '5'
    rxvga2_gain: '0'
    samp_rate: samp_rate
    sdrplay_agc_setpoint: '-30'
    sdrplay_biastee: 'True'
    sdrplay_dabnotch: 'False'
    sdrplay_if_mode: Zero-IF
    sdrplay_rfnotch: 'False'
    settings0: other_settings
    settings1: ''
    stream_args: stream_args
    tia_gain0: '0'
    tia_gain1: '0'
    tune_args0: tune_args
    tune_args1: ''
    tuner_gain0: '10'
    type: fc32
    vga_gain0: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [240, 72.0]
    rotation: 0
    state: true
- name: stream_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Soapy Stream arguments
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1120, 268.0]
    rotation: 0
    state: enabled
- name: tune_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Soapy Channel Tune arguments
    short_id: ''
    type: str
    value: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1296, 268.0]
    rotation: 0
    state: enabled
- name: virtual_sink_0
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [920, 188.0]
    rotation: 0
    state: true
- name: virtual_sink_1
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [680, 348.0]
    rotation: 0
    state: disabled
- name: virtual_source_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 452.0]
    rotation: 0
    state: true
- name: waterfall_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: /tmp/waterfall.dat
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 780.0]
    rotation: 0
    state: enabled
- name: xmlrpc_server_0
  id: xmlrpc_server
  parameters:
    addr: 127.0.0.1
    alias: ''
    comment: ''
    port: '8080'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 708.0]
    rotation: 0
    state: disabled

connections:
- [analog_quadrature_demod_cf_0_0, '0', low_pass_filter_1, '0']
- [analog_quadrature_demod_cf_0_0_0_0, '0', blocks_moving_average_xx_0, '0']
- [blocks_delay_0, '0', blocks_multiply_xx_0, '0']
- [blocks_moving_average_xx_0, '0', blocks_vco_c_0, '0']
- [blocks_multiply_const_vxx_0, '0', fec_extended_decoder_0, '0']
- [blocks_multiply_xx_0, '0', low_pass_filter_0, '0']
- [blocks_vco_c_0, '0', blocks_multiply_xx_0, '1']
- [dc_blocker_xx_0, '0', digital_clock_recovery_mm_xx_0, '0']
- [digital_binary_slicer_fb_0, '0', satnogs_frame_decoder_0, '0']
- [digital_clock_recovery_mm_xx_0, '0', blocks_multiply_const_vxx_0, '0']
- [digital_clock_recovery_mm_xx_0, '0', digital_binary_slicer_fb_0, '0']
- [fec_extended_decoder_0, '0', satnogs_frame_decoder_0_0, '0']
- [low_pass_filter_0, '0', analog_quadrature_demod_cf_0_0, '0']
- [low_pass_filter_0_0, '0', analog_quadrature_demod_cf_0_0_0_0, '0']
- [low_pass_filter_1, '0', dc_blocker_xx_0, '0']
- [satnogs_doppler_compensation_0, '0', virtual_sink_0, '0']
- [satnogs_frame_decoder_0, out, satnogs_json_converter_0, in]
- [satnogs_frame_decoder_0, out, satnogs_udp_msg_sink_0, in]
- [satnogs_frame_decoder_0, out, satnogs_udp_msg_sink_0_1, in]
- [satnogs_frame_decoder_0_0, out, satnogs_json_converter_0_0, in]
- [satnogs_frame_decoder_0_0, out, satnogs_udp_msg_sink_0, in]
- [satnogs_frame_decoder_0_0, out, satnogs_udp_msg_sink_0_1, in]
- [satnogs_json_converter_0, out, satnogs_multi_format_msg_sink_0, in]
- [satnogs_json_converter_0_0, out, satnogs_multi_format_msg_sink_0, in]
- [satnogs_tcp_rigctl_msg_source_0, freq, blocks_message_debug_0, print]
- [satnogs_tcp_rigctl_msg_source_0, freq, satnogs_doppler_compensation_0, doppler]
- [satnogs_tcp_rigctl_msg_source_0, freq, virtual_sink_1, '0']
- [soapy_source_0, '0', qtgui_freq_sink_x_0, '0']
- [soapy_source_0, '0', satnogs_doppler_compensation_0, '0']
- [virtual_source_0, '0', blocks_delay_0, '0']
- [virtual_source_0, '0', low_pass_filter_0_0, '0']
- [virtual_source_0, '0', satnogs_iq_sink_0, '0']

metadata:
  file_format: 1
