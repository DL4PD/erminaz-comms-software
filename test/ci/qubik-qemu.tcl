spawn gdb-multiarch -iex "set arch arm" -ex "target remote :1234" -ex "break HAL_InitTick" -ex "cont" build-qemu/qubik-comms-sw.elf
set timeout 25
set state 1
set error 1
expect {
	"(gdb)" {
		if { $state == 2 } {
			send "kill\n"
			set state 3
		} elseif { $state == 3 } {
			send "quit\n"
			exit $error
		} elseif { $state == 4 } {
			send "bt\n"
			set state 2
		}
		exp_continue
	}
	"Breakpoint 1," {
		set state 2
		set error 0
		puts "pass"
		exp_continue
	}
	"Kill the program being debugged*" {
		send "y\n"
		exp_continue
	}
	"c to continue without paging" {
		send "q\n"
		exp_continue
	}
	timeout {
		send "\003"
		set state 4
		puts "timeout"
		exp_continue
	}
}

puts "fail"
exit 1
